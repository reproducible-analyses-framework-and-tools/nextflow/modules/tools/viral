#!/usr/bin/env nextflow



include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { procd_fqs_to_alns } from '../alignment/alignment.nf'

include { star_index } from '../star/star.nf'
include { star_index as star_index_human } from '../star/star.nf'
include { star_index as star_index_virus } from '../star/star.nf'
include { star_map } from '../star/star.nf'
include { star_map as star_map_human } from '../star/star.nf'
include { star_map as star_map_virus } from '../star/star.nf'
include { awk_get_unaligned } from '../virdetect/virdetect.nf'
include { awk_unaligned_bam_to_fastqs } from '../virdetect/virdetect.nf'
include { count_star_viral_alignments } from '../virdetect/virdetect.nf'

workflow manifest_to_viruses {
// require:
//   MANIFEST
//   params.viral$manifest_to_virues$fq_trim_tool
//   params.viral$manifest_to_virues$fq_trim_tool_parameters
//   params.viral$manifest_to_virues$aln_tool
//   params.viral$manifest_to_virues$aln_tool_parameters
//   params.viral$manifest_to_virues$viral_workflow
//   params.viral$manifest_to_virues$viral_workflow_parameters
//   params.viral$manifest_to_virues$rna_ref
//   params.viral$manifest_to_virues$viral_ref
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    viral_workflow
    viral_workflow_parameters
    rna_ref
    viral_ref
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_viruses(
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      viral_workflow,
      viral_workflow_parameters,
      rna_ref,
      viral_ref,
      manifest_to_raw_fqs.out.fqs)
//  emit:
//    herv_counts = procd_fqs_to_hervquant.out.herv_counts
}

workflow raw_fqs_to_viruses {
// require:
//   params.viral$manifest_to_virues$fq_trim_tool
//   params.viral$manifest_to_virues$fq_trim_tool_parameters
//   params.viral$manifest_to_virues$aln_tool
//   params.viral$manifest_to_virues$aln_tool_parameters
//   params.viral$manifest_to_virues$viral_workflow
//   params.viral$manifest_to_virues$viral_workflow_parameters
//   params.viral$manifest_to_virues$rna_ref
//   params.viral$manifest_to_virues$viral_ref
//   FQS
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    viral_workflow
    viral_workflow_parameters
    rna_ref
    viral_ref
    fqs
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_viruses(
      aln_tool,
      aln_tool_parameters,
      viral_workflow,
      viral_workflow_parameters,
      rna_ref,
      viral_ref,
      raw_fqs_to_procd_fqs.out.procd_fqs)
}

workflow procd_fqs_to_viruses {
// require:
//   params.viral$manifest_to_virues$aln_tool
//   params.viral$manifest_to_virues$aln_tool_parameters
//   params.viral$manifest_to_virues$viral_workflow
//   params.viral$manifest_to_virues$viral_workflow_parameters
//   params.viral$manifest_to_virues$rna_ref
//   params.viral$manifest_to_virues$viral_ref
//   PROCD_FQS
  take:
    aln_tool
    aln_tool_parameters
    viral_workflow
    viral_workflow_parameters
    rna_ref
    viral_ref
    procd_fqs
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      rna_ref,
      params.gtf)
    alns_to_viruses(
      procd_fqs_to_alns.out.alns,
      viral_workflow,
      viral_workflow_parameters,
      viral_ref)
//  emit:
//    herv_counts = salmon_aln_quant.out.quants
//    herv_bams = hervquant_filter_alignments.out.bams
}

workflow alns_to_viruses {
// require:
//   ALNS
//   params.viral$manifest_to_virues$viral_workflow
//   params.viral$manifest_to_virues$viral_workflow_parameters
//   params.viral$manifest_to_virues$viral_ref
  take:
    alns
    viral_workflow
    viral_workflow_parametersa
    viral_ref
  main:
    if( viral_workflow =~ /virdetect/ ) {
      star_index_virus(
        viral_ref,
        '--genomeSAindexNbases 7')
      awk_get_unaligned(
        alns)
      awk_unaligned_bam_to_fastqs(
        awk_get_unaligned.out.bams)
      star_map_virus(
        awk_unaligned_bam_to_fastqs.out.fqs,
        star_index_virus.out.idx_files,
        '--outFilterMismatchNmax 4 --outFilterMultimapNmax 1000 --limitOutSAMoneReadBytes 1000000 --outSAMtype BAM SortedByCoordinate',
        params.dummy_file)
      count_star_viral_alignments(star_map_virus.out.alns)
  }
  emit:
    viral_alns = star_map_virus.out.alns
    viral_counts = count_star_viral_alignments.out.viral_counts
    unaligned_fqs =  awk_unaligned_bam_to_fastqs.out.fqs
}



workflow unaligned_fqs_to_virdetect_cds_counts {
// require:
//   UNALIGNED_READS
  take:
    fqs
    viral_cds_ref
  main:
    star_index_virus(
      viral_cds_ref,
      '--genomeSAindexNbases 7')
    star_map_virus(
      fqs,
      star_index_virus.out.idx_files,
//      '--outFilterMismatchNmax 4 --outFilterMultimapNmax 1000 --limitOutSAMoneReadBytes 1000000 --outSAMtype BAM SortedByCoordinate',
      '--outFilterMismatchNmax 3 --outFilterMultimapNmax 1 --limitOutSAMoneReadBytes 1000000 --outSAMtype BAM SortedByCoordinate',
      params.dummy_file)
    count_star_viral_alignments(star_map_virus.out.alns)
  emit:
    viral_cds_alns = star_map_virus.out.alns
    viral_cds_counts = count_star_viral_alignments.out.viral_counts
}
